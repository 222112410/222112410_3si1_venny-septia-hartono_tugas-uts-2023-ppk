package com.BukuAlumni.BukuAlumni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BukuAlumniApplication {

	public static void main(String[] args) {
		SpringApplication.run(BukuAlumniApplication.class, args);
	}

}
